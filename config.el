;;; $DOOMDIR/config.el -*- lexical-binding: t; -*-

(setq user-full-name "CasperHammer12"
      user-mail-address "mail2gauravprajapati@gmail.com")

(setq doom-font (font-spec :family "Iosevka Nerd Font Complete Mono Windows Compatible" :size 14 :weight 'regular)
      doom-variable-pitch-font (font-spec :family "Red Hat Display" :size 15)
      doom-big-font(font-spec :family "Iosevka Nerd Font Complete Mono Windows Compatible" :size 16)
      doom-unicode-font(font-spec :family "JetBrains Mono" :size 20)
      doom-serif-font(font-spec :family "Roboto Slab" :weight 'light))

(setq doom-theme 'doom-moonlight)

;;
;; (let ((alternatives '("splash-1.png" "splash-2.png" "splash-3.png")))
   
;;  (setq fancy-splash-image
;;        (concat "~/.emacs.d/banners/"
;;                (nth (random (length alternatives)) alternatives))))

(setq-hook! '+doom-dashboard-mode-hook evil-normal-state-cursor (list nil))

(custom-set-faces!
  '(mode-line :family "Red Hat Display")
  '(mode-line-inactive :family "Red Hat Display"))

(setq display-time-mode t)

(display-battery-mode 1)

(after! company
  (setq company-idle-delay 0.5
        company-minimum-prefix-length 2)
  (setq company-show-numbers t)

(setq emojify-emoji-set "twemoji-v2")

(after! centaur-tabs
  (centaur-tabs-mode -1)
  (setq ;;centaur-tabs-height 36
        centaur-tabs-set-icons t
        centaur-tabs-set-bar 'above
        centaur-tabs-gray-out-icons 'buffer)
  (centaur-tabs-change-fonts "Red Hat Display" 100))

(setq org-directory "~/Desktop/org-docs")

(global-subword-mode 1)                           ; iterateThroughCameCaseWords
(setq undo-limit 10000000)                         ; Raise undo-limit to 10Mb
;;      auto-save-default t)
